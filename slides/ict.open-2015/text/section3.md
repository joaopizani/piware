# Planning


## What we already have


### Hardware description combinators

 * Circuits built by structural combination
    + Some constructors:
       - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{Gate}
       - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{seq}
       - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{par}
       - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{Plug}

### Hardware description combinators

 * Parameterized by _gate library_ and _atomic type_
    + Examples: **boolean**, arithmetic, crypto

 * Fundamental gates have their behaviour _specified_
    + Instead of "calculated"
    + VHDL for fundamental gates also _given_

### Functional semantics

 * Turns a circuit description into a _function_ over bit vectors
    + _Executable_
    + In contrast to previous work with _relational_ semantics

 * Allows for circuit _simulation_
    + Also works with sequential circuits
       - _Stream_ semantics
       - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{stream-semantics-sig}

### Combinational case study

 * Parallel prefix circuits
    + Produce _prefixes_ of expressions like $(a_{1} + a_{2} + \ldots + a_{n})$
    + The binary operator `_+_` is _associative_
    + Associativiy means that order is irrelevant → _parallel_

 * M.Sc experimentation project
    + Modelling this family of circuits using Π-Ware primitives
    + Proving algebraic properties of the family as a whole
    + Lead to important revisions of the DSL


## What is planned

### Compilation to VHDL

 * Work in progress
    + Agda representation of VHDL (abstract syntax) – Done
    + Pretty-printing
    + Translating \AY{(}\AD{ℂ} \AB{i} \AB{o}\AY{)} to \AY{(}\AD{VHDLEntity} \AB{i} \AB{o}\AY{)}

 * Will work towards generating _hierarchical_ VHDL (vs. _flat_)
    + Naming is a challenge
    + Can result in changes to _core_ combinators
       - Ex: nested `seqs` vs. `fold` constructor

### Randomized/exhaustive testing

 * Similar to Haskell's QuickCheck

 * Special case: exhaustive testing = proof of correctness
    + For specific circuit size, not infinite family

### Sequential circuit proofs

 * Framework of properties for sequential circuits

 + Larger case study to validate:
    + Validade library as a whole
    + Parts of a general-purpose computer?

