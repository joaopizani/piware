# Context


## Definition

### One-sentence definition
A unified DSL (Π-Ware) embedded in _Agda_ for _modelling_ hardware circuits,
_synthesizing_ them and _proving_ properties about their behaviour and structure.


## Motivation

### Hardware is growing

Constantly changing algorithms/standards

 * Qualcomm Snapdragon 810 _SoCs_
    + DSP / Video decoding (H.264 / H.265)
    + Bluetooth / 4G / Wifi – 802.11 b/g/n/ac/d (60 GHz)
 * Intel/AMD x86 extensions
    + AES-NI
    + RdRand
 * Xilinx Zynq (ARM + FPGA)

 * Accelerating trend
    + Miniaturization continues for a while (Moore's law)
    + **But** CPU architecture optimization has diminishing returns\ \cite{dark-silicon}

### Hardware design is hard
Hardware design is a _complex_ and _expensive_ activity:

 * We already try to make software more principled
    + Hardware needs it even more

 * More performance requirements, more optimization
    + More error-prone development process

 * Mistakes found after production are more serious
    + No such thing as an "update" to a chip
    + Thus the need for extensive validation / verification
       - Up to 50% of total development costs


## Problems

### Hardware design "status quo"

_Fragmentation_: Different languages for different design tasks...

 * Simulation
    + SystemC, VHDL/Verilog
 * Synthesis
    + VHDL/Verilog, C/C++ (subsets)
 * Verification:
    + SAT solvers (circuits of _specific_ size)
    + Theorem provers (of a handwritten _model_)

Each _manual translation_ increases the chance of _losing invariants_.

### Hardware design "status quo"

Imagine if we had the same situation for software:

 * To interpret (simulate) a program, you **must** use `Python`

 * For compilation to machine code, **only** `C` works
    + What is "compilable" `C` depends _a lot_ on the compiler

 * For verification, translate manually to `Hol` / `ACL2` / `Coq`

### Functional hardware description

Functional programming has _already_ been used to help hardware design (since the 1980s).

 * First, _independent_ DSLs (such as μFP, Bluespec)

 * Then, as _embedded_ DSLs in functional languages
    + Ex: _Lava_\ \cite{lava1998} (embedded in Haskell)

 * Solves (almost) the fragmentation problem
    + Simulation, synthesis, diagram generation

### Functional hardware description

Functional programming has _already_ been used to help hardware design (since the 1980s).

 * Verification still not integrated
    + _Lava_ uses external SAT solver

 * Type system _cannot_ express some guarantees
    + Sizing: \mintinline{haskell}{addNBit :: [Bit] -> [Bit] -> [Bit]}

