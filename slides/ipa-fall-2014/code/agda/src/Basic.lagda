\begin{code}
open import Data.Nat using (ℕ; suc; _+_)
open import Data.Vec using (Vec; []; _∷_; _++_)
open import Data.Bool using (Bool)
open import Data.Product using (_×_; _,_; ∃₂)
open import Relation.Binary.PropositionalEquality using (_≡_)
\end{code}


\begin{code}
pattern 1+ n = suc n

postulate undefined : ∀ {ℓ} {α : Set ℓ} → α

α : Set
α = Bool
\end{code}

%<*head>
\begin{code}
head : ∀ {n} → Vec α (1+ n) → α
head (x ∷ _) = x
\end{code}
%</head>

%<*splitAt-noproof-sig>
\begin{code}
splitAt : ∀ n {m}  → Vec α (n + m)
                   → Vec α n × Vec α m
\end{code}
%</splitAt-noproof-sig>
%<*splitAt-noproof-def>
\begin{code}
splitAt = undefined
\end{code}
%</splitAt-noproof-def>

%<*splitAt-proof-sig>
\begin{code}
splitAt' : (n m : ℕ) → (xs : Vec α (n + m))
                     → ∃₂ λ (ys : Vec α n) (zs : Vec α m) → xs ≡ ys ++ zs
\end{code}
%</splitAt-proof-sig>
%<*splitAt-proof-def>
\begin{code}
splitAt' = undefined
\end{code}
%</splitAt-proof-def>
