
DIRGUARD=@mkdir -p $(@D)

MAIN_NAME=main
REFERENCES_NAME=references
LATEXMKOPTS=-pdf -f -quiet
TEXT_PARTS_SRC_ROOT=text
TEXT_PARTS_SRC_GENTEX=$(TEXT_PARTS_SRC_ROOT)/tex-generated

MAINTEX=$(TEXT_PARTS_SRC_ROOT)/$(MAIN_NAME).tex
MAINPDF=./$(MAIN_NAME).pdf
REFERENCES=$(REFERENCES_NAME).bib
TEXT_PARTS=section1 \
           section2 \
           section3


CODE_AGDA_ROOT=code/agda
CODE_AGDA_SRC=$(CODE_AGDA_ROOT)/src
CODE_AGDA_GENTEX=$(CODE_AGDA_ROOT)/latex
CODE_AGDA_STY=$(CODE_AGDA_GENTEX)/agda.sty
CODE_AGDA_STDLIB=${HOME}/build/agda/lib/current/src

CODE_AGDA_MODULES=Basic \
                  PiWare/Gates \
                  PiWare/Gates/BoolTrio \
                  PiWare/Circuit/Core \
                  PiWare/Circuit \
                  PiWare/Samples/AndN \
                  PiWare/ProofSamples/AndN


all: $(MAINPDF)

$(MAINPDF): \
	$(REFERENCES) $(MAINTEX) \
	$(TEXT_PARTS:%=$(TEXT_PARTS_SRC_GENTEX)/%.tex) \
	$(CODE_AGDA_STY) \
	$(CODE_AGDA_MODULES:%=$(CODE_AGDA_GENTEX)/%.tex)
	latexmk $(LATEXMKOPTS) $(MAINTEX)

$(TEXT_PARTS_SRC_GENTEX)/%.tex: $(TEXT_PARTS_SRC_ROOT)/%.md
	$(DIRGUARD); pandoc $< -t beamer -o $@

$(CODE_AGDA_GENTEX)/%.tex: $(CODE_AGDA_SRC)/%.lagda
	$(DIRGUARD); agda --allow-unsolved-metas -i $(CODE_AGDA_STDLIB) -i $(CODE_AGDA_SRC) \
		--latex-dir=$(CODE_AGDA_GENTEX) --latex $<


clean:
	latexmk -c $(MAINTEX)
	rm -f $(TEXT_PARTS:%=$(TEXT_PARTS_SRC_GENTEX)/%.tex)
	find $(TEXT_PARTS_SRC_GENTEX) -type d -empty -delete

veryclean: clean
	rm -f $(CODE_AGDA_MODULES:%=$(CODE_AGDA_GENTEX)/%.tex)
	find $(CODE_AGDA_GENTEX) -type d -empty -delete
	rm -f $(MAINPDF)

.PHONY: clean veryclean all

