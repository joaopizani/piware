# Proposal


## Goals

### What we want to achieve
Build a DSL (Π-Ware) for hardware description in a _dependently-typed_ programming language (Agda).

 * The stronger type system should allow us to:
    + Ban _malformed_ circuits _by design_
    + Write proofs of circuit behaviour and structure
        - Also for (infinite) families of circuits

 * Examples:
    + Correct sizing:
        - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{addN-correct-sizing}
    + No "floating" wires:
        - \ExecuteMetaData[code/agda/excerpts/PiWare.tex]{seq-no-floating}


## Tools

### What dependent types are good for

 * Strong static guarantees, less "corner cases"
    + \ExecuteMetaData[code/agda/excerpts/Basic.tex]{head}
    + \ExecuteMetaData[code/agda/excerpts/Basic.tex]{splitAt-noproof-sig}

 * Proofs about program behaviour
     + Because of _Curry-howard correspondence_
     + \ExecuteMetaData[code/agda/excerpts/Basic.tex]{splitAt-proof-sig}

### How dependent types help us

  * Consistent _sizing_ information
     + Example: structural combination constructors

  * Structural induction proofs for _infinite_ families of circuits
     + Ex: \ExecuteMetaData[code/agda/excerpts/PiWare/ProofSamples/AndN.tex]{andN-core-ident-left-sig}

  * Coinduction for semantics of sequential circuits
     + Work in progress: how to express meaningful properties
     + Temporal logic?

