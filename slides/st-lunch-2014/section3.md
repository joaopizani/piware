Challenges
==========

## Current / foreseen challenges ##

Related to synthesis

 * Correct translation to `VHDL`
    + Intermediary "sized netlist" type
 * _Naming_ in the generated code

## Current / foreseen challenges ##

 * Automated _testing_ of circuit behaviour (QuickCheck-like)

 * Proof automation

 * Automated serialization/deserialization of Agda types

 * Prove (algebraic) properties of combinators _in Agda_
    + Ex: \AI{\_>>\_} together with \AI{Nil} form a monoid

