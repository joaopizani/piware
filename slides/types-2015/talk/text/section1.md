Context
=======

Definition
----------

### One-sentence definition ###

A unified DSL (Π-Ware) embedded in _Agda_ for _modeling_ hardware circuits,
_synthesizing_ them and _proving_ properties about their behaviour and structure.


Motivation
----------

### Hardware is growing ###

More specifically, hardware _acceleration_. Three reasons why:

 * Miniaturization still has some generations to go\ \cite{itrs}
 * Microarch. optimization gives diminishing returns\ \cite{dark-silicon}
 * Battery energy density vs. demand for computation

More applications benefit from _hardware acceleration_

 * DSP, crypto, codecs, graphics, comm. protocols, etc.

Hardware design benefits more from _rigour_

 + _Early optimization_, more error-prone
 * Mass production, less _updateable_

### Hardware design "status quo" ###

Myriad of languages for specific design tasks...

 * **Simulation:** SystemC, VHDL/Verilog
 * **Synthesis:** VHDL/Verilog (subsets), C/C++ (subsets)
 * **Verification:** SAT solvers / Theorem provers

Problems:

 * Manual translation
 * Loss of invariants, manual checking

An analogous situation in software seems bizarre:

 * To "simulate" (interpret) your program, you use Haskell
 * For compilation to x86, use C (non-standardized)

### Functional hardware DSLs ###

 * Solve **most** of the problems with multiple descriptions

 * "Popular" example: Lava (Chalmers)
    + Description, simulation, testing in Haskell
    + Verification through external SAT solver

 * Drawbacks:
    + Non modular verification (fully-automated)
       - Only for _specific_ circuits (not _families_)
    + Haskell types not expressive enough
        - \mintinline{haskell}{addN :: Int -> ([Bit], [Bit]) -> [Bit]}
        - Could use lots of extensions, but why compromise?


Why Agda?
---------

### Dependent types for hardware ###

 * Well-formedness
   + Rule out design mistakes _early_
      - Floating wires (matching interfaces)
      - Short-circuits (\AI{Plug} constructor)

\centerline{\includegraphics[width=0.6\textwidth]{imgs/plug-seq-disconnected-input.pdf}}

   + More precise specification of circuit _generators_
      - **Haskell:** \mintinline{haskell}{addN :: Int -> ([Bit], [Bit]) -> [Bit]}
      - **Agda:** \AF{addN} \AY{:} \AY{(}\AB{n} \AY{:} \AD{ℕ}\AY{)} \AY{→} \AD{C} \AY{(}\AN{2} \AO{*} \AB{n}\AY{)} \AY{(}\AI{suc} \AB{n}\AY{)}

 * Mainly: proofs in the same language as the models
    + (Functional) correctness proofs
    + Provably-correct circuit _transformations_

