Π-Ware
======

Syntax
------

### Circuit syntax ###

 * Π-Ware is _deep-embedded_
    + Multiple semantics, algebraic manipulation

 * Low-level, _architectural_ representation
    + Analogous to a block diagram
    + Untyped, but _sized_

\ExecuteMetaData[code/agda/latex/PiWare/Circuit.tex]{Circuit-nodelay}

### Circuit syntax ###

 * Combinational / sequential
    + Single way of constructing a sequential circuit: \AI{DelayLoop}

\ExecuteMetaData[code/agda/latex/PiWare/Circuit.tex]{Circuit-delay}

 * The \AD{ℂ} type is "tagged" to keep the two cases distinct
    + The distinction is mainly important for simulation
    + Easier definitions of _generators_
    + \ExecuteMetaData[code/agda/latex/PiWare/Circuit.tex]{Circuit-predecl}
      \ExecuteMetaData[code/agda/latex/PiWare/Circuit.tex]{IsComb}
    + Obs: \AI{ω} has to do with $\Sigma^\omega$

### Fundamental gates ###

 * Circuits are built by combining smaller circuits
    + Ultimately, from a library of _fundamental_ \ARR{Gates}
    + Each gate specified by a _function_ over (binary) _words_

\begin{code}
\>[4]\AT{8}{}\<[8]%
\>[8]\AF{andSpec} \AY{:} \AD{Vec} \AD{Bool} \AN{2} \AY{→} \AD{Vec} \AD{Bool} \AN{1}\<%
\\
\>[4]\AT{8}{}\<[8]%
\>[8]\AF{andSpec} \AY{(}\AB{x} \AI{∷} \AB{y} \AI{∷} \AI{ε}\AY{)} \AY{=} \AF{[} \AB{x} \AF{∧} \AB{y} \AF{]}\<%
\end{code}

 * A "traditional" instance of \ARR{Gates} is \AF{BoolTrio}
    + Set of gates: $\{ ⊥, ⊤, ¬, ∧, ∨ \}$
    + With the usual specification (stdlib)

 * Other "interesting" instances:
    + Modular arithmetic
    + Cryptographic primitives
    + Primitives for _scans_ (case study)

### Fundamental gates ###

 * To define a gate library, we need to define:
    + How many gates are there
    + Each gate's _interface_
    + Each gate's _specification_

\begin{code}%
\>[4]\AT{8}{}\<[8]%
\>[8]\AL{|in|} \AL{|out|} \<[20]%
\>[20]\AY{:} \AD{Gate\#} \AY{→} \AD{ℕ}\<%
\\
\>[4]\AT{8}{}\<[8]%
\>[8]\AL{spec} \<[20]%
\>[20]\AY{:} \AY{(}\AB{g} \AY{:} \AD{Gate\#}\AY{)} \AY{→} \AY{(}\AF{W} \AY{(}\AB{|in|} \AB{g}\AY{)} \AY{→} \AF{W} \AY{(}\AB{|out|} \AB{g}\AY{)}\AY{)}\<%
\end{code}

 * Dependent types help us again
    + The \AD{Gate\#} type ranges in $[0 .. n-1]$
    + \AL{spec} works over words of the _right size_

### Atomic types ###

 * The whole \AM{Circuit} module is parameterized by a record
    + Defining what is _carried_ over the "wires"
    + \AF{W} \AY{=} \AD{Vec} \AL{Atom}

 * This \ARR{Atomic} class is similar to Haskell's \ARR{Enum}
    + An atomic type needs to be _finite_
    + There's a bijection between the type and $[0 .. n-1]$
        - \AL{enum} \AY{:} \AD{Fin} \AB{|Atom|} \AF{↔} \AB{Atom}
        - In Agda, the bijection is _proven_

 * Dependent types move runtime errors to type checking:
    + **Haskell**: \mintinline{haskell}{succ maxBound} → runtime error
    * **Agda**: "\mintinline{haskell}{succ maxBound}" → doesn't typecheck!

### Atomic types (\AD{Bool}) ###

 * Some possible instances...
    + \AD{Bool}
    + Multi-valued logics (VHDL's `std_logic`)
    + States of a state machine

 * Simplest "useful": \AD{Bool}
    + We use the mapping $0 ↔ False; 1 ↔ True$
    + Order and choice of indices _don't matter_

 * Later how this parameterization ties into _synthesis_

### Putting all pieces together ###

 * Small circuit using \AD{Bool} atoms and \AF{BoolTrio} gates

\centerline{\includegraphics[width=0.6\textwidth]{imgs/xor-sample.pdf}}

\ExecuteMetaData[code/agda/latex/Basic.tex]{example-low-level}

### Data abstraction ###

 * Sometimes it's more convenient to have _typed_ circuit I/O
    + Used for conveniently-typed testing/simulation
    + \AD{ℂ} \AY{(}\AD{Bool} \AD{×} \AD{Bool}\AY{)} \AD{Bool}  instead of  \AD{ℂ} \AL{2} \AL{1}

 * To be used as circuit I/O, a type needs to be \ARR{Synthesizable}
    + Have a mapping to vectors of \AD{Atom}s (a.k.a _words_)

\ExecuteMetaData[code/agda/latex/PiWare/Synthesizable.tex]{Synth}

 * Instances
    + Currently: \AD{\_×\_}, \AD{\_⊎\_}, \AD{Vec}, primitives.
    + Future: datatype-generic approach


Semantics
---------

### Circuit semantics ###

 * Our goal is to have two semantics:
    + Behavioural (done)
    + Structural/Synthesis (TODO)

 * Our behavioural semantics is _functional_
    + From a circuit, a _function_ is derived
    + Circuits can be "run" or simulated over inputs

. . .

 * Two kinds of simulation: _combinational_ and _sequential_
    + **Combinational:** no internal state
       - \ExecuteMetaData[code/agda/latex/PiWare/Simulation.tex]{simulation-combinational}
       - _Tagged_ with \AI{σ} (has no \AI{DelayLoop})

    + Example: \AF{⟦} \AF{and} \AF{⟧} \AY{(}\AI{true} \AI{∷} \AI{false} \AI{∷} \AI{ε}\AY{)} \AY{≡} \AF{[} \AI{false} \AF{]}

### Sequential simulation ###

 * More general, for circuit with (possibly) internal state
    + Simulation works over infinite sequences
    + Modeled using Agda's \AD{Stream} (coinductive)

 * User interface
   + \ExecuteMetaData[code/agda/latex/PiWare/Simulation.tex]{simulation-sequential}
      - Works on **both** sequential and combinational circuits
      - Ex: \AF{⟦} \AF{not} \AF{⟧ω} \AY{(}\AF{repeat} \AF{[} \AI{false} \AF{]}\AY{)} \AD{≈} \AF{repeat} \AF{[} \AI{true} \AF{]}

 * \AD{Stream} functions can "look into the future"
    + We use a _causal stream function_
       - \AB{f} \AY{=} \AF{run} \AF{∘} \AB{f´} with \AB{f´} a _step_  (past × present → next)
       - Idea from Tarmo Uustalu's paper\ \cite{essence-dataflow-programming}


Proofs
------

### Proving circuit properties ###

 * What can be proven: depends on which semantics is used
    * **Structural**: "the circuit size grows linearly with input size"
    * **Behavioural:** "the circuit will never produce value X"

 * Example behavioural property: _functional correctness_
    + Agreement with a _specification_ on all inputs
    + Specification is a _function_
    + Ex: \AY{∀} \AY{(}\AB{x} \AB{y} \AY{:} \AD{Int8}\AY{)} \AY{→} \AF{⟦} \AF{add₂₅₆} \AF{⟧} \AY{(}\AB{x} \AI{,} \AB{y}\AY{)} \AD{≡} \AB{x} \AF{+₂₅₆} \AB{y}

### Properties of circuit combinators ###

 * Circuit combinators have _algebraic_ properties
    + \{ \AI{\_⟫\_}, \AF{id⤨} \} forms an (indexed) monoid
    + \{ \AI{\_∥\_}, \AF{nil⤨} \} forms an (indexed) monoid
    * \AB{f} \AF{∘} \AB{g} \AD{≅} \AF{id} \AY{→} \AI{Plug} \AB{g} \AI{⟫} \AI{Plug} \AB{f} \AD{≋} \AF{id⤨}

 * Developed a notion of equivalence between circuits (\AD{\_≋\_})
    + Equality _up to a certain semantics_ (simulation)
        - Proven equivalence relation
        - Equational reasoning
    + Combinators are _congruences_ (\AI{\_⟫\_}, \AI{\_∥\_}, \AI{Plug}, _custom_)
    + Equal behaviour → opportunity for _optimization_

 * Π-Ware can be used to define whole _classes_ of circuits
    + With their own associated laws...

