Present / Future
================

Current work
------------

### Current work ###

Finishing up...

 * Case study: parallel-prefix circuits
    + Computes $[a₁, (a₁ + a₂), (a₁ + a₂ + a₃), ...]$ in parallel
    + Behaviour similar to Haskell's \mintinline{haskell}{scanl}
    + Applications: sorting, addition, filters, etc., etc.

 * General class + examples implemented in Π-Ware
    + Inspired by Ralf Hinze's "An algebra of scans"\ \cite{algebra-scans}
    + As M.Sc experimentation project (Yorick Sijsling)
    + Ex. law: \AF{scan} \AY{(}\AI{suc} \AB{m}\AY{)} \AF{▱} \AF{scan} \AY{(}\AI{suc} \AB{n}\AY{)} \AF{≋} \AF{scan} \AY{(}\AB{m} \AO{+} \AI{suc} \AB{n}\AY{)}

 * "Side-effects" of the project

### Current work ###

Partly there / beginning...

 * Correctness of sequential circuits
    + Temporal logic
    + Difficulties with _bisimilarity_ (\AD{\_≈\_})
        - Better _carrier_ than \AY{(}\AD{Stream} \AB{α} \AY{→} \AD{Stream} \AB{β}\AY{)}?

 * Little testing framework

 * "Automated" checking for any _specific_ circuit
    + Given a (trivial) proofs, one for each possible input
    + Produce proof of generalized statement
    + Already working for \AD{Fin} \AB{n}
       - \ExecuteMetaData[code/agda/latex/FinForAll.tex]{elimFin-type}

Future
------

### Future ###

 * Translation to VHDL
    + Simplified, intermediary language
    + Two _key_ additions to the framework
       - In \ARR{Atomic}: VHDL type, one VHDL expression per value
       - In \ARR{Gates}: one VHDL component per gate

 * Optimizations in generated VHDL
    + Try to use circuit laws to justify "rewrite" steps
    + Example: $((a₁ ∧ a₂) ∧ a₃) ∧ a₄ ≅ (a₁ ∧ a₂) ∧ (a₃ ∧ a₄)$

 * Automation possibilities
    + Testing input generation
    + Congruence "generation"
    + Monoid solver for circuit equality

