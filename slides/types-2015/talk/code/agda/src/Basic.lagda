\begin{code}
module Basic where

open import Function using (id)
open import Data.Nat using (ℕ; zero; suc; _*_)
open import Data.Vec using (Vec; []; concat; _++_; splitAt) renaming (_∷_ to _∷′_)
open import Data.Product using (∃; _,_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

open import PiWare.Gates.BoolTrio using (BoolTrio; ¬ℂ#; ∧ℂ#; ∨ℂ#)
open import PiWare.Gates using (module Gates)
open import PiWare.Circuit {Gt = BoolTrio} using (ℂ; 𝐂; Gate; Plug; _⟫_; _∥_)
open import PiWare.Plugs.Core using (fork×⤪)
open import PiWare.Plugs BoolTrio using (id⤨)
open import PiWare.Samples.BoolTrioComb using (¬ℂ; ∧ℂ; ∨ℂ)
\end{code}


%<*Vect-head>
\begin{code}
data Vect (α : Set) : ℕ → Set where
    ε    : Vect α zero
    _∷_  : ∀ {n} → α → Vect α n → Vect α (suc n)

head : ∀ {α n} → Vect α (suc n) → α
head (x ∷ xs) = x
\end{code}
%</Vect-head>


%<*group-decl>
\begin{code}
group : ∀ {α : Set} n k  → (xs : Vec α (n * k))
                         → ∃ λ (xss : Vec (Vec α k) n) → xs ≡ concat xss
\end{code}
%</group-decl>
%<*group-def>
\begin{code}
group zero k [] = ([] , refl)
group (suc n) k xs                  with splitAt k xs
group (suc n) k .(ys ++ zs)         | (ys , zs , refl) with group n k zs
group (suc n) k .(ys ++ concat zss) | (ys , ._ , refl) | (zss , refl) = ((ys ∷′ zss) , refl)
\end{code}
%</group-def>


\begin{code}
fork⤨ : 𝐂 2 4
fork⤨ = Plug (fork×⤪ {2})
\end{code}


%<*example-low-level>
\begin{code}
⊻ℂ : 𝐂 2 1
⊻ℂ =   fork⤨
     ⟫ (¬ℂ ∥ id⤨ ⟫ ∧ℂ)  ∥  (id⤨ {1} ∥ ¬ℂ ⟫ ∧ℂ)
     ⟫ ∨ℂ
\end{code}
%</example-low-level>

