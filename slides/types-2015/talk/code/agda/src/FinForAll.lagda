\begin{code}
module FinForAll where

open import Function using (_$_; _∘_)
open import Data.Unit using (⊤; tt)
open import Data.Empty using (⊥)
open import Data.Fin using (Fin) renaming (zero to Fz; suc to Fs)
open import Data.Vec using (lookup; map; allFin; replicate)
open import HVec using (Vec↑; ε̂; _◁̂_; lookup↑; replicate↑)

open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong)
\end{code}


\begin{code}
postulate lookup∘map∘allFin : ∀ {n ℓ} {α : Set ℓ} (f : Fin n → α) (i : Fin n) → lookup i (map f $ allFin n) ≡ f i
\end{code}

%<*elimFin-type>
\AgdaTarget{elimFin}
\begin{code}
elimFin : ∀ {n} {P : Fin n → Set} → Vec↑ n $ map P (allFin n) → (∀ i → P i)
\end{code}
%</elimFin-type>
%<*elimFin-def>
\begin{code}
elimFin {P = P} ps i with lookup↑ i ps | lookup∘map∘allFin P i
elimFin {P = P} ps i | Pi              | eq-Pi rewrite eq-Pi = Pi
\end{code}
%</elimFin-def>

\begin{code}
silly : Fin 3 → Fin 3
silly Fz                = Fz
silly (Fs Fz)           = Fz
silly (Fs (Fs Fz))      = Fz
silly (Fs (Fs (Fs ())))
\end{code}

\begin{code}
silly-const : ∀ i → silly i ≡ Fz
silly-const Fz                = refl
silly-const (Fs Fz)           = refl
silly-const (Fs (Fs Fz))      = refl
silly-const (Fs (Fs (Fs ())))
\end{code}

\begin{code}
silly-const' : ∀ i → silly i ≡ Fz
silly-const' = elimFin (refl ◁̂ refl ◁̂ refl ◁̂ ε̂)
\end{code}

-- The use of decidable **equality** means we can only talk about **equality** of Fins
-- It should be possible to extend this approach to other **decidable relations** (unary, binary) in general
\begin{code}
decEq : ∀ {n} (i i′ : Fin n) → Set
decEq Fz     Fz      = ⊤ 
decEq Fz     (Fs i′) = ⊥
decEq (Fs i) Fz      = ⊥
decEq (Fs i) (Fs i′) = decEq i i′
\end{code}

\begin{code}
soundness : ∀ {n} {i i′ : Fin n} → decEq i i′ → i ≡ i′
soundness {i = Fz}   {Fz}   _  = refl
soundness {i = Fz}   {Fs _} ()
soundness {i = Fs _} {Fz}   ()
soundness {i = Fs i} {Fs i′}   = cong Fs ∘ soundness
\end{code}

\begin{code}
units : ∀ {n} → Vec↑ n (replicate ⊤)
units = replicate↑ tt
\end{code}

\begin{code}
silly-const″ : ∀ i → silly i ≡ Fz
silly-const″ = soundness ∘ elimFin units
\end{code}
