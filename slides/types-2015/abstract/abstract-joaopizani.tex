\documentclass{easychair}

\usepackage{fontspec}

%% Standard packages
\usepackage{authblk}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{color}

\usepackage{agda}

%% PDF metainformation
\usepackage{datetime}
\usepackage{ifpdf}
\ifpdf
\pdfinfo{
    /Author (Joao Paulo Pizani Flor)
    /Title (PiWare: An Embedded Hardware Description Language using Dependent Types)
    /Keywords (HDL, DSL, EDSL, Hardware description, Dependently-typed programming, Agda, DTP)
    /CreationDate (D:\pdfdate)
}
\fi


%% LaTeX meta-information
\title{Π-Ware: An Embedded Hardware Description Language using Dependent Types}
\titlerunning{Hardware Description with Dependent Types}

\author{
    João Paulo Pizani Flor
\and
    Wouter Swierstra
}
\authorrunning{João Pizani, Wouter Swierstra}

\institute{
   Utrecht University\\
   Utrecht - The Netherlands\\
   \email{\{j.p.pizaniflor,w.s.swierstra\}@uu.nl}\\
}



%% The document itself
\begin{document}
    \maketitle

    \section*{Extended abstract}

    Computer hardware has experienced a steady exponential increase in complexity in the last decades.
    There is an increased demand for application-specific integrated circuits that avoid the
    proverbial \emph{von Neumann bottleneck}~\cite{backus-lecture},
    and ever more algorithms enjoy hardware acceleration
    (such as 3D/2D renderers, video/audio codecs, cryptographic primitives and network protocols).

    This demand puts pressure on the industry to make the hardware design process quicker.
    At the same time, hardware design also requires very strong correctness guarantees.
    These strong correctness requirements are commonly met using (exhaustive) testing and
    model checking, making the design-verify-fix loop slower and more costly than in the software world.

    There is a long tradition~\cite{gammie2013,sheeran2005} of modeling hardware using functional programming
    to pursue higher productivity and stronger correctness assurance.
    A particular trend is to use functional programming languages as \emph{hosts} for
    Embedded Domain-Specific Languages (EDSLs) aimed at hardware description,
    of which Lava~\cite{lava1998} is a popular example.
    Some of the limitations of these hardware EDSLs are due to the host's type system,
    which lacks the power to express some desirable properties of circuit models.

    We believe that the advantages brought to hardware design by FP-inspired techniques
    can be even greater if we use dependent types.
    We define a Hardware Description Language (HDL) called Π-Ware,
    which is \emph{embedded} in the dependently-typed general-purpose programming language Agda.
    Circuits described in Π-Ware can be simulated, transformed in several ways,
    proven correct, and synthesized (work in progress).

    The existence of several different semantics for circuit models is due to Π-Ware's \emph{deep embedding}:
    There is an inductive datatype of circuits (called \AgdaDatatype{ℂ}),
    and semantics (simulation, synthesis, gate count, etc.) are just functions with \AgdaDatatype{ℂ} as domain.
    More specifically, the circuit datatype is \emph{indexed} by two natural numbers representing,
    respectively, the \emph{sizes} of the circuit's input and output.
    The arithmetic fine tuning of these indices, along with other features of dependent types,
    allow us to ``ban'' certain classes of design mistakes \emph{by construction}, for example:
    \begin{itemize}
        \item Our circuit constructors guarantee that all circuits are \emph{well-sized}, i.e.,
            there are never ``floating'' or ``dangling'' wires.
        \item Connections between circuits are guaranteed to never cause \emph{short-circuits}
            (two or more sources connected to the same load).
    \end{itemize}

    In addition to this \emph{structural correctness by construction},
    we can also \emph{interactively} prove properties of circuits,
    which provides some advantages over the fully-automated verification approach used widely in industry.
    First of all, we can inductively prove properties over whole \emph{circuit families}.
    Furthermore, proofs written in Agda are \emph{modular}, in contrast to fully-automated verification.
    This means that, when proving laws concerning a certain circuit (family),
    we \emph{reuse} previously proven facts about its constituent subcircuits.

    In particular, we can write proofs of \emph{functional correctness}.
    Our simulation semantics (even for sequential circuits) are \emph{executable}
    (in contrast to other EDSLs~\cite{coquet} with a \emph{relational} semantics).
    This means that the simulation semantics simply converts each circuit into a function,
    which can then be applied to input vectors, producing output vectors.
    We can then formulate the statement of whether a circuit (family) \emph{implements} the behaviour
    of a given \emph{specification function},
    and proofs of correctness can be written by induction on circuit inputs.

    As a first step in exploring the possibilities that our approach offers,
    we conducted a case study, aiming to formalize a class of circuits
    known as \emph{parallel-prefix sums}~\cite{hinze-algebra-scans} in Π-Ware.
    Parallel-prefix circuit combinators have been defined \emph{in terms of Π-Ware primitives},
    and we are proving several properties of this class of circuits
    which had previously only been postulated or proven on paper.

    The case study is leading us to establish suitable \emph{equivalence relations} between circuits.
    We have defined a relation of \emph{equality up to simulation}, which identifies
    any two circuits with the same simulation behaviour (taking equal inputs to equal outputs).
    Reasoning about this equivalence relation, without postulating function extensionality, presents
    several challenges, for which we have initial progress towards satisfactory solutions.
    We are also working on the definition of suitable \emph{algebraic} laws
    involving circuit constructors and combinators.
    These may be used to describe \emph{semantic-preserving} circuit transformations.

    Currently, all our research involving \emph{circuit equivalence}, circuit algebraic laws
    and proof combinators is focused on \emph{combinational} circuits (with no internal state).
    While it is already possible to define \emph{and simulate} sequential circuits in Π-Ware,
    we have not yet thoroughly considered a logical framework for reasoning about
    the behaviour of – and transformations between – sequential circuits.

    All in all, we claim there are several benefits to be gained from
    using dependent types to describe hardware circuits. These
    techniques are more widely applicable to other domains and,
    therefore, we believe them to be of interest to the wider TYPES community.


    \bibliographystyle{plain}
    \bibliography{references}

\end{document}
