Π-Ware
======

Syntax
------

### Circuit syntax ###

 * Low-level, _architectural_ representation
 * Untyped, but _well-sized_

. . .

\ExecuteMetaData[code/agda/latex/PiWare/Circuit/Core.tex]{Circuit-core}

 * No _floating_ wires, no short circuits

### Atomic types ###

 * The whole \AM{Circuit} module is parameterized by a record
    + Defining what is _carried_ over the "wires"

 * This \ARR{Atomic} class is similar to Haskell's \ARR{Enum}
    + An atomic type needs to be _finite_
    + There's a mapping between the type and $\{0..n\}$
       - "\mintinline{haskell}{toEnum}" / "\mintinline{haskell}{fromEnum}"

 * Dependent types move runtime errors to compile-time:
    + **Haskell**: \mintinline{haskell}{succ maxBound} → runtime error
    * **Agda**: "\mintinline{haskell}{succ maxBound}" → doesn't typecheck!

### Atomic types (\AD{Bool}) ###

 * Several interesting instances possible
    + \AD{Bool}
    + \AD{Int8}, \AD{Int16}, \AD{IntN}...
    + States of a state machine (any enumerated type)

 * Simplest "useful": \AD{Bool}
    + We use the mapping $0 → False; 1 → True$
    + Order and choice of indices _don't matter_


### Fundamental gates ###

 * Circuits are built by combining smaller circuits
    + Ultimately, from a library of _fundamental_ \ARR{Gates}

 * To define a gate library, we need to define:
    + How many gates are there
    + Each gate's _interface_
    + Each gate's _specification_

\begin{code}%
\>[4]\AgdaIndent{8}{}\<[8]%
\>[8]\AgdaField{|in|} \AgdaField{|out|} \<[20]%
\>[20]\AgdaSymbol{:} \AgdaDatatype{GateIdx} \AgdaSymbol{→} \AgdaDatatype{ℕ}\<%
\\
\>[4]\AgdaIndent{8}{}\<[8]%
\>[8]\AgdaField{spec} \AgdaSymbol{:} \AgdaSymbol{(}\AgdaBound{g} \AgdaSymbol{:} \AgdaDatatype{GateIdx}\AgdaSymbol{)} \AgdaSymbol{→} \AgdaSymbol{(}\AgdaFunction{W} \AgdaSymbol{(}\AgdaBound{|in|} \AgdaBound{g}\AgdaSymbol{)} \AgdaSymbol{→} \AgdaFunction{W} \AgdaSymbol{(}\AgdaBound{|out|} \AgdaBound{g}\AgdaSymbol{)}\AgdaSymbol{)}\<%
\end{code}

 * Dependent types help us again
    + The \AD{GateIdx} type ranges in $\{0..n\}$
    + The function returned by \AL{spec} works over words of the _right size_

### Fundamental gates ###

 * A "traditional" instance of \ARR{Gates} is \AF{BoolTrio}
    + Set of gates: $\{ ⊥, ⊤, ¬, ∧, ∨ \}$
    + With the usual specification functions (from the stdlib)

 * Other "interesting" instances:
    + Modular arithmetic
    + Cryptographic primitives

### Putting all pieces together ###

 * Small circuit using \AD{Bool} atoms and \AF{BoolTrio} gates

\centerline{\includegraphics[width=0.6\textwidth]{imgs/xor-sample.pdf}}

\begin{code}%
\>\AgdaFunction{⊻ℂ} \AgdaSymbol{:} \AgdaDatatype{ℂ'} \AgdaNumber{2} \AgdaNumber{1}\<%
\\
\>\AgdaFunction{⊻ℂ} \AgdaSymbol{=} \<[8]%
\>[8]\AgdaFunction{pFork}\<%
\\
\>[0]\AgdaIndent{5}{}\<[5]%
\>[5]\AgdaInductiveConstructor{⟫} \AgdaSymbol{(}\AgdaFunction{¬ℂ} \AgdaInductiveConstructor{|} \AgdaFunction{pid} \AgdaInductiveConstructor{⟫} \AgdaFunction{∧ℂ}\AgdaSymbol{)} \<[29]%
\>[29]\AgdaInductiveConstructor{|} \<[33]%
\>[33]\AgdaSymbol{(}\AgdaFunction{pid} \AgdaInductiveConstructor{|} \AgdaFunction{¬ℂ} \AgdaInductiveConstructor{⟫} \AgdaFunction{∧ℂ}\AgdaSymbol{)}\<%
\\
\>[0]\AgdaIndent{5}{}\<[5]%
\>[5]\AgdaInductiveConstructor{⟫} \AgdaFunction{∨ℂ}\<%
\end{code}

### Data abstraction ###

 * Sometimes it's more convenient to have _typed_ circuit I/O
    + \AD{ℂ} \AY{(}\AD{Bool} \AD{×} \AD{Bool}\AY{)} \AD{Bool}  instead of  \AD{ℂ'} \AL{2} \AL{1}

 * To be used as circuit I/O, a type needs to be \ARR{Synthesizable}
    + Have a mapping to vectors of \AD{Atom}s (a.k.a _words_)

\ExecuteMetaData[code/agda/latex/PiWare/Synthesizable.tex]{Synth-down-up}


Semantics
---------

### Circuit semantics ###

 * Our goal is to have two semantics:
    + Behavioural (done)
    + Structural (TODO)

 * Our behavioural semantics is _functional_
    + From a circuit, a _function_ is derived
    + Circuits can be "run" or simulated over inputs

. . .

 * Two kinds of simulation: _combinational_ and _sequential_
    + **Combinational:** no internal state
       - \AF{⟦\_⟧'} \AY{:} \AY{(}\AB{c} \AY{:} \AD{ℂ'} \AB{i} \AB{o}\AY{)} \AY{\{}\AB{p} \AY{:} \AF{comb'} \AB{c}\AY{\}} \AY{→} \AY{(}\AF{W} \AB{i} \AY{→} \AF{W} \AB{o}\AY{)}
       - Example: \AF{⟦} \AF{and} \AF{⟧'} \AY{(}\AI{true} \AI{∷} \AI{false} \AI{∷} \AI{ε}\AY{)}

### Sequential simulation ###

 * More general, for circuit with (possibly) internal state
    + Simulation takes a _stream_ of inputs

 * Modeled using Agda's \AD{Stream}
    + Type of infinite lists, defined by using _coinduction_

 * User interface:
    + \AF{⟦\_⟧*'} \AY{:} \AD{ℂ'} \AB{i} \AB{o} \AY{→} \AY{(}\AD{Stream} \AY{(}\AF{W} \AB{i}\AY{)} \AY{→} \AD{Stream} \AY{(}\AF{W} \AB{o}\AY{)}\AY{)}
     - Example: \AF{⟦} \AF{mapS} \AF{not} \AF{⟧*'} \AY{(}\AF{repeat} \AF{[} \AI{false} \AF{]}\AY{)}

 * Implementation detail
    + General \AD{Stream} functions can "look into the future"
    + Our implementation uses only _causal stream functions_


Proofs
------

### Proving circuit properties ###

 * What can be proven: depends on which semantics is used
    * **Structural**: "the circuit size grows linearly with input size"
    * **Behavioural:** "the circuit will never produce value X"

. . .

 * We are particularly interested in _functional correctness_
    + Agreement with a _specification_ on all inputs
    + Specification is a _function_
    + Example: \AY{∀} \AY{(}\AB{x} \AB{y} \AY{:} \AD{Int8}\AY{)} \AY{→} \AF{⟦} \AF{add₂₅₆} \AF{⟧} \AY{(}\AB{x} \AI{,} \AB{y}\AY{)} \AD{≡} \AB{x} \AF{+₂₅₆} \AB{y}

### Properties of circuit combinators ###

 * Circuit combinators have _algebraic_ properties
    + \AI{\_⟫\_} (seq) is associative and has identity \AF{pid} (monoid)
    * \AI{\_||\_} (par) is also a monoid, with identity \AI{Nil}
    * \AB{f} \AF{∘} \AB{g} \AD{≡} \AF{id} \AY{→} \AI{Plug} \AB{g} \AI{⟫} \AI{Plug} \AB{f} \AD{≅} \AF{pid}

 * Agda is perfect for proving such statements
    + With a special notion of equality between circuits (\AD{≅})
       - _Equality up to simulation_
    + Equal behaviour → opportunity for _optimization_

 * Π-Ware can be used to define whole _classes_ of circuits
    + With their own associated laws...

