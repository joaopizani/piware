Context
=======

Definition
----------

### One-sentence definition ###

A unified DSL (Π-Ware) embedded in _Agda_ for _modelling_ hardware circuits,
_synthesizing_ them and _proving_ properties about their behaviour and structure.


Motivation
----------

### Hardware is growing ###

More specifically, hardware _acceleration_

 * Miniaturization still has a decade to go\ \cite{itrs}
    + Microarch. optimization shows diminishing returns\ \cite{dark-silicon}

 * More applications benefit from _hardware acceleration_
    + DSP, crypto, codecs, graphics, comm. protocols, etc.
    + Many more could benefit if hardware design wasn't so _hard_

. . .

 * Hardware benefits more from _rigour_
    + Mass production, less _updateable_, lots of optimization
    + More error-prone, errors are more serious

### Hardware design "status quo" ###

Myriad of languages for specific design tasks...

 * **Simulation:** SystemC, VHDL/Verilog
 * **Synthesis:** VHDL/Verilog, C/C++ (subsets)
 * **Verification:** SAT solvers / Theorem provers

. . .

The same situation in software seems bizarre nowadays:

 * To "simulate" (interpret) your program, you use Haskell
 * For compilation to x86, use C (non-standardized)

### Functional hardware DSLs ###

 * Solve **most** of the problem with multiple descriptions

 * "Popular" example: Lava (Chalmers) (Ex.)
    + Description, simulation, testing in Haskell
    + Verification through external SAT solver
       - Non-modular proofs

 * Also, Haskell types are not as strong as we want
    + \mintinline{haskell}{addN :: Int -> ([Bit], [Bit]) -> [Bit]}


Why Agda?
---------

### Depedent types ###

Why program using dependent types?

 * Less runtime errors
    + More _correctness by construction_

 * Reasoning about your programs
    + In the _same language_ of the program itself

### Dependent types for hardware ###

 * Better specification of sizing constraints
    + **Haskell:** \mintinline{haskell}{addN :: Int -> ([Bit], [Bit]) -> [Bit]}
    + **Agda:** \AF{addN} \AY{:} \AY{(}\AB{n} \AY{:} \AD{ℕ}\AY{)} \AY{→} \AD{C} \AY{(}\AN{2} \AO{*} \AB{n}\AY{)} \AY{(}\AI{suc} \AB{n}\AY{)}

 * Rule out design mistakes
    + Ex: short-circuits are ill-typed

 * Correctness proofs in the same language as the model

