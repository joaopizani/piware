Present / Future
================

Current work
------------

### Current work ###

 * Case study: parallel-prefix circuits
    + Computes $[a₁, (a₁ + a₂), (a₁ + a₂ + a₃), ...]$ in parallel
    + Behaviour similar to Haskell's \mintinline{haskell}{scanl}

 * General class + examples implemented in Π-Ware
    + As M.Sc experimentation project (Yorick Sijsling)
    + Proving associated laws in Agda
    + Inspired by Ralf Hinze's "An algebra of scans"\ \cite{algebra-scans}

### Current work ###

 * Correctness of sequential circuits
    + Temporal logic

. . .

 * Translation to VHDL
    + Simplified, intermediary language
    + Two _key_ additions to the framework
       - In \ARR{Atomic}: VHDL type, one VHDL expression per value
       - In \ARR{Gates}: one VHDL component per gate


Future
------

### Future ###

 * "Proofs for free" for any _specific_ circuit
    + Given a collection of proofs, one for each possible input
    + Generate the generalized statement
    + Using _reflection_ to make it all easy to use

. . .

 * Optimizations in generated VHDL
    + Try to use circuit laws to justify "rewrite" steps
    + Example: $((a₁ ∧ a₂) ∧ a₃) ∧ a₄ ≅ (a₁ ∧ a₂) ∧ (a₃ ∧ a₄)$

