
+ Low-level decisions: serial vs. parallel, sizes of I/O, etc.

                % Ex: DSP for sensors, accel. SSL, etc.

                %  + More and early need for verification

                    % via VHDL or EDIF

                    % Forbid some "non-sensical" circuits

                    % Implements a certain function, circuit optimizations, etc.

                % if NOT S then A else B

          % Multiple semantics easy (sim, area, delay)

              % Implicit tag

              % 2 sources to the same load (total function o → i)

  %  + Simple version (direct, no fold)

            % Circuit becomes an Agda function with appropriate domain and codomain

        % Pardon my sloppy Agda-like notation

            % Basically, a finite type

            % Functional for now, generalizing later...

                % This happens a lot, we came to this in case studies

                % For now: Agda's vector equality, and bisimilarity with vectors as elements

                % Column-major order vs. row-major order

            % As in Haskell's scan function

        %  + Circuits with DelayLoop as mealy machines

        %  + Simple example: reg

        % Will show props later, after semantics

        %  * The semantics

                % Default = at the beginning of time

        %  * Some general properties

        %  * Register properties
